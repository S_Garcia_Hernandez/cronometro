library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity contar10decimas_tb is
end entity;

architecture test of contar10decimas_tb is

  -- Inputs
  signal Reset2, CLK2 ,E1decima: std_logic;

  -- Outputs
  signal S1segundo : std_logic;
  signal Sdecimas :   std_logic_vector(3 downto 0);
  
  component contar10decimas is
    port (
        E1decima:  in std_logic;
        CLK2:      in std_logic;
        Reset2:    in std_logic;
        S1segundo: out std_logic;
        Sdecimas:   out std_logic_vector
    );
  end component;

  constant CLK_PERIOD : time := 1 sec / 100_000_000;  -- Clock period

begin
  -- Unit Under Test
  uut: contar10decimas
    port map (
     E1decima  => E1decima,
     CLK2  => CLK2,
     Reset2    => Reset2,
     S1segundo  => S1segundo,
     Sdecimas    => Sdecimas
    );
    
E1GEN: process
  begin
     E1decima <= '0';
    wait for 8* CLK_PERIOD;
     E1decima <= '1';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
  clkgen: process
  begin
    CLK2 <= '0';
    wait for 0.5 * CLK_PERIOD;
    CLK2 <= '1';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
    Reset2 <= '0' after 0.25 * CLK_PERIOD,
               '1' after 20 * CLK_PERIOD,
               '0' after 21 * CLK_PERIOD;
               
  tester: process
  begin
    wait until  Reset2 = '1';
    for i in 1 to 300 loop
      wait until CLK2 = '0';
    end loop;
  
    wait for 0.25 * CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;
