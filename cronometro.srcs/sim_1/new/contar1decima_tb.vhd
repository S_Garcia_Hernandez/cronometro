
-- Code your testbench here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity contar1decima_tb is
end entity;

architecture test of contar1decima_tb is

  -- Inputs
  signal Reset1, CLK1 : std_logic;

  -- Outputs
  signal S1decima : std_logic;

  component contar1decima is
    port (
      CLK1: in std_logic;        
    Reset1 : in std_logic;
    S1decima: out std_logic     --salida el reloj ha contado una decima de segundo
    );
  end component;

  constant CLK_PERIOD : time := 1 sec / 100_000_000;  -- Clock period

begin
  -- Unit Under Test
  uut: contar1decima
    port map (
     CLK1  => CLK1,
     Reset1    => Reset1,
      S1decima    => S1decima
    );

  clkgen: process
  begin
    CLK1 <= '0';
    wait for 0.5 * CLK_PERIOD;
    CLK1 <= '1';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
    Reset1 <= '0' after 0.25 * CLK_PERIOD;
              
  tester: process
  begin
    wait until  Reset1 = '1';
    for i in 1 to 300 loop
      wait until CLK1 = '0';
    end loop;
  
    wait for 0.25 * CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;