LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY cronometro_tb IS
END cronometro_tb;
 
ARCHITECTURE behavior OF cronometro_tb IS 
 
    COMPONENT TOP
    PORT(
         clk100Mhz : IN  std_logic;
         boton_reset : IN  std_logic;
         segment : OUT  std_logic_vector(6 downto 0);
         ANsel : OUT  std_logic_vector(3 downto 0);
         SEG_DP : OUT  std_logic
        );
    END COMPONENT;

   --Inputs
   signal clk100Mhz  : std_logic := '0';
   signal boton_reset : std_logic := '0';

 	--Outputs
   signal segment : std_logic_vector(6 downto 0);
   signal  ANsel : std_logic_vector(3 downto 0);
   signal SEG_DP : std_logic;

   -- Clock period definitions
   constant Clk_period : time := 10 ns;
 
BEGIN
 
	-- Unidad en pruebas (UUT: Unit Under Test)
   uut: TOP PORT MAP (
          clk100Mhz =>clk100Mhz,
          boton_reset => boton_reset,
          segment => segment,
          ANsel =>  ANsel,
          SEG_DP => SEG_DP
        );

   -- Generacion del reloj
   Clk_process :process
   begin
      clk100Mhz <= '0';
      wait for Clk_period/2;
      clk100Mhz <= '1';
      wait for Clk_period/2;
   end process;

   -- Reset
   stim_proc: process
   begin	
       boton_reset <= '0';
      wait for 32 ns;	
       boton_reset <= '1';
      wait for 42 ns;	
       boton_reset <= '0';	
      wait for 8700 ns;	
      boton_reset <= '1';
      wait for 50 ns;	
      boton_reset <= '0';
      wait for 60 us;	
      boton_reset <= '1';
      wait for 6 us;	
      boton_reset <= '0';
      wait;
   end process;

END;
