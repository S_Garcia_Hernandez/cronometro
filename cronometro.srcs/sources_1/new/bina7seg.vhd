
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bina7seg is
 Port ( 
        code: in std_logic_vector(3 downto 0);  --bin input
        led: out std_logic_vector(6 downto 0)  --display 7 seg
 );
end bina7seg;

architecture Behavioral of bina7seg is
begin
with code select 
        led <=  "0000001" WHEN "0000", --0:A,B,C,D,F
				"1001111" WHEN "0001", -- 1: B,C
				"0010010" WHEN "0010", -- 2: A,B,D,E,G
				"0000110" WHEN "0011", -- 3: A,B,C,D,G
				"1001100" WHEN "0100", --4
				"0100100" WHEN "0101",
				"0100000" WHEN "0110",
				"0001111" WHEN "0111",
				"0000000" WHEN "1000",
				"0000100" WHEN "1001",
				"1111110" WHEN others;

end Behavioral;
