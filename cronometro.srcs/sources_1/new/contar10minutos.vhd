

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity contar10minutos is
    Port (
        CLK      : in std_logic;    
        reset    : in std_logic;
        e60seg   : in std_logic;
        CE       : in std_logic;
        A_D      : in std_logic;
        eavisoa  : in std_logic;
        sminutos : out std_logic_vector(3 downto 0)
     );
end contar10minutos;

architecture Behavioral of contar10minutos is
     signal reg : unsigned( sminutos'range);
     
begin
     P_CONTAR10minutos: Process (reset, CLK)
        begin
         if reset = '1' then
            reg <= (others => '0');
        elsif CLK'event and CLK='1' then
            if CE='1' then
                if  e60seg  = '1' then
                     if reg = "1001" then
                        reg <= (others => '0');
                    else
                        if (A_D= '1')then
                            reg <= reg + 1;
                        end if;
                    end if;    
                end if;
                  if eavisoa='1' then         
                       if reg=0 then  
                            reg <= "1001";
                        else 
                                reg <= reg - 1;
                        end if;
                 end if;
            end if;           
       end if;
    end process; 
sminutos <= std_logic_vector(reg);
end Behavioral;
