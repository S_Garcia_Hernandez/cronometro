library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity contar10seg is
 Port (
    s10segundos: out std_logic;
    CLK:         in std_logic;
    reset:       in std_logic;
    e1seg :      in std_logic;
    CE:          in std_logic;
    A_D:         in std_logic;
    eavisoa:     in std_logic;
    savisoa:     out std_logic;
    ssegundos:   out std_logic_vector(3 downto 0)
 );
end contar10seg;

architecture Behavioral of contar10seg is
     signal reg : unsigned( ssegundos'range);
     signal s10segundos_i: std_logic;
     signal savisoa_i:std_logic;
begin
     P_CONTA10SEGUNDOS: Process (reset, CLK)
        begin
         if reset = '1' then
            reg <= (others => '0');
            savisoa<='0';
        elsif CLK'event and CLK='1' then
            if CE='1' then
                if  e1seg = '1' then
                    if s10segundos_i ='1' then
                        reg <= (others => '0');
                    else
                        if (A_D= '1') then
                            reg <= reg + 1;    
                         end if;
                     end if;
                 end if;
                 if eavisoa='1' then 
                    if reg=0 then
                        reg <= "1001";
                    else 
                             reg <= reg - 1;
                      end if;
                   end if;
                  end if;
            end if;
    end process; 
ssegundos <= std_logic_vector(reg);
s10segundos <= s10segundos_i;
savisoa <= savisoa_i;
S10segundos_i <= '1' when e1seg='1' and reg=9 and A_D='1' else '0';
savisoa_i <= '1' when A_D='0' and reg = 0 and eavisoa='1' else '0';
end Behavioral;
