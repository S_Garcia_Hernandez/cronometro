
--decodificador 2 a 4 para seleccionar que anodo de que display esta activo cada milisegundo
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decodificador2_4 is
Port ( 
    I: in std_logic_vector(1 downto 0); --entrada de control
    AN: out std_logic_vector(7 downto 0);
    PD:out std_logic
);
end decodificador2_4;

architecture Behavioral of decodificador2_4 is

begin
    P_DECOD: Process (I)
    begin
        case I is
        when "00"  =>
            AN <= "11111110";
            PD <='1';
        when "01" =>
            AN <= "11111101";
            PD <='0';
        when "10" =>
            AN <= "11111011";
            PD <='1';
        when others => -- "11"
            AN <= "11110111";
            PD <='0';
        end case;
    end process; 
   
end Behavioral;
