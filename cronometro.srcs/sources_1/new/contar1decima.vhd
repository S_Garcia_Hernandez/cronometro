

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity contar1decima is
 Port ( 
    CLK     : in std_logic;        
    Reset   : in std_logic;
    CE      : in std_logic;
    S1decima: out std_logic     --salida el reloj ha contado una decima de segundo
 );
end contar1decima;

architecture Behavioral of contar1decima is
    signal cuenta : natural range 0 to 2**27-1; --cuenta hasta 100*10^6 27bits
    constant cfincuenta : natural := 100; -- solo para la simulacion(comentar para sintesis)
    --constant cfincuenta : natural := 10000000; -- 100MHz(descomentar para la sintesis)
    signal s1decima_i:std_logic;
begin
     -- Proceso que genera la senal periodica de 1 decima de segundo
     P_conta1seg: Process (Reset, CLK)
        begin
            if Reset = '1' then
                cuenta <= 0;
            elsif CLK'event and CLK = '1' then
                if CE='1' then 
                    if s1decima_i = '1' then 
                        cuenta <= 0;
                    else
                        cuenta <= cuenta + 1;
                    end if;
                end if;
            end if;
       end process;   
       S1decima_i <= '1' when cuenta=cfincuenta-1 else '0';
       S1decima <= S1decima_i;
end Behavioral;
