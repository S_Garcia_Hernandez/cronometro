
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Multiplexor is
 Port ( 
    cuenta4miliseg:    in std_logic_vector(1 downto 0); --entrada de selccion
    decimasdesegundo:  in std_logic_vector(3 downto 0); --entrada de datos
    segundos:          in std_logic_vector(3 downto 0); --entrada de datos
    decenasdesegundos: in std_logic_vector(3 downto 0); --entrada de datos
    minutos:           in std_logic_vector(3 downto 0); --entrada de datos
    mostrar:           out std_logic_vector(3 downto 0) --salida del multiplexor
    
    
 );
end Multiplexor;

architecture Behavioral of Multiplexor is

begin
    P_MUX: Process ( decimasdesegundo,segundos,decenasdesegundos,minutos,cuenta4miliseg)
    begin
    case  cuenta4miliseg is
        when "00" =>
            mostrar <= decimasdesegundo;
        when "01" =>
             mostrar <= segundos;
         when "10" =>
              mostrar <= decenasdesegundos;
         when others =>
              mostrar <= minutos;       
         end case;
    end process; 

end Behavioral;
