library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity TOP is
 Port (
    boton_reset:in std_logic;                    -- boton push cpu_reset
    clk100Mhz :in std_logic;                     --reloj interno FPGA
    Switches  :in std_logic_vector(1 downto 0);  --switches(0)='0' paro(1 start)  switches(1)='1' hacia delante (0 atras) 
    segment : out std_logic_vector(6 downto 0);  --leds del display 7 segmentos
    ANsel : out std_logic_vector(7 DOWNTO 0);    --seleccion del anodo activo
    SEG_DP : OUT  std_logic                      --punto decimal display 4 y 2
 );
end TOP;

architecture behavioral of TOP is



    signal SYNC_IN_i : std_logic;
    signal edge_i : std_logic;
    signal S1decima_i :std_logic;
    signal S1segundo_i: std_logic;
    signal Sdecimas_i: std_logic_vector(3 downto 0); 
    signal s10segundos_i: std_logic;
    signal ssegundos_i:  std_logic_vector(3 downto 0); 
    signal  s60segundos_i: std_logic;
    signal sdecenasdesegundos_i: std_logic_vector(3 downto 0); 
    signal sminutos_i: std_logic_vector(3 downto 0); 
    signal  S1miliseg_i: std_logic;
    signal Scuenta4milis_i: std_logic_vector(1 downto 0);
    signal mostrar_i:std_logic_vector(3 downto 0); 
    signal  savisoa_i:std_logic;
    signal  savisoa10seg: std_logic;
    signal  savisoa60seg: std_logic;
    signal SW_i:std_logic_vector(1 downto 0); 
    
    COMPONENT sincronizador
        PORT (
            CLK      : in std_logic;
            ASYNC_IN : in std_logic;
            SYNC_OUT : out std_logic
        );
    END COMPONENT;

    COMPONENT EDGEDTCTR
        PORT (
            clk     : in std_logic;
            SYNC_IN : in std_logic;
            EDGE    : out std_logic
        );
    END COMPONENT;
    
     COMPONENT contar1decima
        PORT (
            CLK      : in std_logic;        
            Reset    : in std_logic;
            CE       : in std_logic;
            S1decima : out std_logic 
            
        );
    END COMPONENT;
    
     COMPONENT contar10decimas
        PORT (
            E1decima  :in std_logic;
            CLK       :in std_logic;
            Reset     :in std_logic;
            A_D       :in std_logic;
            CE        :in std_logic;
            savisoa   :out std_logic;
            S1segundo :out std_logic;
            Sdecimas  :out std_logic_vector(3 downto 0) 
        );
    END COMPONENT;
    
     COMPONENT contar10seg
        PORT (
            s10segundos :out std_logic;
            CLK         :in std_logic;
            reset       :in std_logic;
            e1seg       :in std_logic;
            CE          :in std_logic;
            A_D         :in std_logic;
            eavisoa     :in std_logic;
            savisoa     :out std_logic;
            ssegundos   :out std_logic_vector(3 downto 0) 
        );
    END COMPONENT;
    
    COMPONENT contar60segundos
        PORT (
          e10segundos         :in std_logic;
          CLK                 :in std_logic;
          reset               :in std_logic;
          CE                  :in std_logic;
          A_D                 :in std_logic;
          eavisoa             :in std_logic;
          savisoa             :out std_logic;
          s60segundos         :out std_logic;
          sdecenasdesegundos  :out std_logic_vector(3 downto 0) 
        );
    END COMPONENT;
    
     COMPONENT contar10minutos
        PORT (
            CLK      : in std_logic;    
            reset    : in std_logic;
            e60seg   : in std_logic;
            CE       :in std_logic;
            A_D      :in std_logic;
            eavisoa  : in std_logic;
            sminutos : out std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    
    COMPONENT contar1miliseg
        PORT (
            CLK       :in std_logic;        
            Reset     :in std_logic;
            S1miliseg :out std_logic 
        );
    END COMPONENT;
    
    COMPONENT contar4milis
        PORT (
            E1mili        :in std_logic;                    
            CLK           :in std_logic;                    
            Reset         :in std_logic;                                      
            Scuenta4milis :out std_logic_vector(1 downto 0)  
        );
    END COMPONENT;
    
    COMPONENT decodificador2_4
        PORT (
            I   :in std_logic_vector(1 downto 0); --entrada de control
            AN  :out std_logic_vector(7 downto 0);
            PD  :out std_logic
        );
    END COMPONENT;
    
      COMPONENT Multiplexor
        PORT (
            cuenta4miliseg    :in std_logic_vector(1 downto 0); --entrada de selccion
            decimasdesegundo  :in std_logic_vector(3 downto 0); --entrada de datos
            segundos          :in std_logic_vector(3 downto 0); --entrada de datos
            decenasdesegundos :in std_logic_vector(3 downto 0); --entrada de datos
            minutos           :in std_logic_vector(3 downto 0); --entrada de datos
            mostrar           :out std_logic_vector(3 downto 0) --salida del multiplexor
        );
    END COMPONENT;
    
    COMPONENT bina7seg
        PORT (
           code :in std_logic_vector(3 downto 0);  --bin input
           led  :out std_logic_vector(6 downto 0)  --display 7 seg
        );
    END COMPONENT;
    
    
begin
    sw_i <= switches;
     
    
    Inst_SYNCHRNZR: sincronizador PORT MAP (
        ASYNC_IN =>boton_reset,
        CLK      => clk100MhZ,
        SYNC_OUT =>SYNC_IN_i
    );
    
    Inst_EDGEDTCTR: EDGEDTCTR PORT MAP (
        clk => clk100Mhz,
        SYNC_IN =>SYNC_IN_i,
        EDGE=>edge_i
    );
    
    Inst_contar1decima: contar1decima PORT MAP(
        CLK      =>  clk100Mhz,     
        Reset    => edge_i,
        CE      =>  sw_i(0),
        S1decima => S1decima_i
    );
    
    Inst_contar10decima: contar10decimas PORT MAP(
        E1decima  => S1decima_i,
        CLK       => clk100Mhz,
        Reset     => edge_i,
        A_D       => sw_i(1),
        CE        => sw_i(0),
        savisoa   => savisoa_i,
        S1segundo => S1segundo_i,
        Sdecimas  => Sdecimas_i 
    );
    
    Inst_contar10seg: contar10seg PORT MAP(
        s10segundos => s10segundos_i,
        CLK         => clk100Mhz,
        reset       => edge_i,
        A_D         =>  sw_i(1),
        CE          =>  sw_i(0),
        eavisoa     => savisoa_i,
        savisoa     => savisoa10seg,
        e1seg       => S1segundo_i,
        ssegundos   => ssegundos_i
    );
    
    Inst_contar60seg: contar60segundos PORT MAP(
          e10segundos        => s10segundos_i,
          CLK                => clk100Mhz,
          reset              => edge_i,
          A_D                =>  sw_i(1),
          CE                 =>  sw_i(0),
          eavisoa            => savisoa10seg,
          savisoa            => savisoa60seg,
          s60segundos        =>  s60segundos_i,
          sdecenasdesegundos => sdecenasdesegundos_i
    );
    
    Inst_contar10minutos: contar10minutos PORT MAP(
            CLK      => clk100Mhz,
            reset    => edge_i,
            A_D      => sw_i(1),
            CE       =>  sw_i(0),
            eavisoa  => savisoa60seg,
            e60seg   => s60segundos_i,
            sminutos => sminutos_i
    );
    
    Inst_contar1miliseg: contar1miliseg PORT MAP(
            CLK       => clk100Mhz,
            Reset     => edge_i,
            S1miliseg => S1miliseg_i
    );
    
    Inst_contar4mili: contar4milis PORT MAP(
            E1mili        => S1miliseg_i,               
            CLK           => clk100Mhz,       
            Reset         => edge_i,                          
            Scuenta4milis => Scuenta4milis_i
    );
    
    Inst_de2_4: decodificador2_4 PORT MAP(
            I => Scuenta4milis_i,
            AN => ANsel,
            PD =>SEG_DP
    );
    
     Inst_multiplexor: Multiplexor PORT MAP(
            cuenta4miliseg => Scuenta4milis_i,
            decimasdesegundo => Sdecimas_i,
            segundos => ssegundos_i,
            decenasdesegundos => sdecenasdesegundos_i,
            minutos => sminutos_i,
            mostrar    =>  mostrar_i     
    );
    
    Inst_bina7seg: bina7seg PORT MAP(
          code => mostrar_i,
          led => segment
    );
    
end behavioral;




