
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--La velocidad del cambio del anado del display activo debe ser tan r�pida que
--no sea perceptible. Por ejemplo, si cambiamos el display cada
--milisegundo seguramente no nos demos cuenta de que est� cambiando, salvo que como
--luce menos tiempo, lo veremos lucir con menor intensidad.

entity contar1miliseg is
  Port ( 
         CLK        :in std_logic;        
         Reset      :in std_logic;
         S1miliseg  :out std_logic 
  );
end contar1miliseg;

architecture Behavioral of contar1miliseg is
    signal cuenta : natural range 0 to 2**17-1;
    constant cfincuenta : natural := 100; --solo para simulaci�n(objetivo reducir el tiempo de simulaci�n)
   -- constant cfincuenta : natural := 100000; --descomentar para sintesis
    signal s1seg : std_logic; 
begin
-- Proceso que genera la senal periodica de 1 milisegundo
 P_conta1miliseg: Process (Reset, CLK)
 begin
    if Reset = '1' then
        cuenta <= 0;
        s1miliseg <= '0';
    elsif CLK'event and CLK = '1' then
         if cuenta = cfincuenta-1 then 
            cuenta <= 0;
            s1miliseg <= '1';
          else
            cuenta <= cuenta + 1;
            s1miliseg <= '0';
          end if;
    end if;
 end process; 

end Behavioral;
