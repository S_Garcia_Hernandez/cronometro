
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity contar60segundos is
  Port ( 
    e10segundos :  in std_logic;
    CLK         :  in std_logic;
    reset       :  in std_logic;
    CE          :  in std_logic;
    A_D         :  in std_logic;
    eavisoa     :  in std_logic;
    savisoa     :  out std_logic;
    s60segundos :   out std_logic;
    sdecenasdesegundos: out std_logic_vector(3 downto 0)
  );
end contar60segundos;

architecture Behavioral of contar60segundos is
     signal reg : unsigned( sdecenasdesegundos'range);
     signal s60segundos_i:std_logic;
     signal savisoa_i:std_logic;
begin
     P_CONTAR60SEGUNDOS: Process (reset, CLK)
        begin
         if reset = '1' then
            reg <= (others => '0');
            savisoa<='0';
        elsif CLK'event and CLK='1' then
            if CE='1' then
                if  e10segundos  = '1' then
                    if s60segundos_i='1' then
                        reg <= (others => '0');
                    else
                         if (A_D= '1')then
                            reg <= reg + 1;
                          end if;
                     end if;
                  end if;
                if  eavisoa ='1' then
                    if reg=0 then  
                         reg <= "0101";
                    else 
                         reg <= reg - 1;
                     end if;
                  end if;
                end if;
            end if;
    end process; 
sdecenasdesegundos <= std_logic_vector(reg);
s60segundos <= s60segundos_i;
savisoa <= savisoa_i;
s60segundos_i <= '1' when e10segundos='1' and reg=5 and A_D='1'else '0';
savisoa_i <= '1' when A_D='0' and reg = 0 and eavisoa ='1' and A_D='0'  else '0';
end Behavioral;
