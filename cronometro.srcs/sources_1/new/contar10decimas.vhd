library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity contar10decimas is
    Port (
        E1decima:  in std_logic;
        CLK:      in std_logic;
        Reset:    in std_logic;
        CE:        in std_logic;   --para habilitar el stop
        A_D:        in std_logic;  --cuenta ascendente/descendente
        S1segundo: out std_logic;
        savisoa:    out std_logic;
        Sdecimas:   out std_logic_vector(3 downto 0)  --se�al que se dirige al conversor de 7 segmentos  
    );
end contar10decimas;

architecture Behavioral of contar10decimas is
   signal reg : unsigned( Sdecimas'range);
   signal S1segundo_i:std_logic;
   signal savisoa_i:  std_logic;
   
begin
    P_CONTA10DECIMAS: Process (Reset, CLK)
        begin
         if Reset = '1' then
            reg <= (others => '0');
        elsif CLK'event and CLK='1' then
            if CE='1' then
                if E1decima = '1' then
                    if S1segundo_i = '1' then
                        reg <= (others => '0');
                        --savisoa<='0';
                    else
                        if (A_D= '1')then
                            reg <= reg + 1;
                        else  
                            reg <= reg - 1;
                            --savisoa<='0';
                            if(reg = 0) then
                                --savisoa<='1';
                                reg <= "1001";
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process; 
    
Sdecimas <= std_logic_vector(reg);
S1segundo<=S1segundo_i;
savisoa<=savisoa_i;
S1segundo_i <= '1' when E1decima='1' and reg=9 and A_D='1' else '0';
savisoa_i<='1' when A_D='0' and reg = 0 and E1decima='1' else '0';
end Behavioral;
