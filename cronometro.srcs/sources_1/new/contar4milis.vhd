

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
-- En cada milisegundo se muestra un display distinto (s�lo hay un �nodo activo). 
--La frecuencia de refresco del display es de 250 Hz (T=4ms).

entity contar4milis is
 Port ( 
    E1mili:  in std_logic;                    
    CLK:      in std_logic;                    
    Reset:    in std_logic;                                      
    Scuenta4milis:  out std_logic_vector(1 downto 0)    
 );
end contar4milis;

architecture Behavioral of contar4milis is
     signal reg : unsigned(  Scuenta4milis'range);
begin
 P_CONTA4MILISEGUNDOS: Process (Reset, CLK)
        begin
         if Reset = '1' then
            reg <= (others => '0');
        elsif CLK'event and CLK='1' then
            if E1mili = '1' then
                 reg <= reg + 1;
             end if;
         end if;
    end process; 
Scuenta4milis <= std_logic_vector(reg);

end Behavioral;
